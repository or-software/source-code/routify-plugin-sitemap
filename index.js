console.log('routify-plugin-sitemap loaded...');
const fs = require('fs');
const { SitemapStream, streamToPromise } = require('sitemaps');
const { Readable } = require('stream');
const defaults = { dir: 'dist', static: [], exclude: [], include: [], hreflang: [], baseUrlLang: [], urlLangType: 'subpage', robotsTxt: false };
module.exports = (options) => {
  return {
    name: 'genSitemap',
    build: ({ instance }) => {
      console.log('routify-plugin-sitemap init-ed');
      // An array with your links
      const links = [{ url: '/page-1/', changefreq: 'daily', priority: 0.3 }]
      // Create a stream to write to
      const stream = new SitemapStream({ hostname: 'https://...' })
      // Return a promise that resolves with your XML string
      return streamToPromise(Readable.from(links).pipe(stream)).then((data) =>
        data.toString()
      )
      function clean(value) {
        let cval = encodeURI(value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;'));
        // console.log("Cleaned value:", cval);
        return cval;
      }
      function genUrl(file, o, type) {
        // console.log("Generating URL for", file);
        let url = o.baseUrl.replace(/(?<=https?:\/\/).+/, '') + clean(o.baseUrl.split(/^https?:\/\//)[1] + file.loc).replace(/\/{2,}/, '/');
        if (url.length > 2048) return '';
        let data = `
   <${type}>
      <loc>${url}</loc>
      ${file.lastmod ? `<lastmod>${clean((new Date(file.lastmod)).toISOString())}</lastmod>` : ''}
      ${file.changefreq ? `<changefreq>${clean(file.changefreq)}</changefreq>` : ''}
      ${file.priority != undefined ? `<priority>${Number(file.priority)}</priority>` : ''}`;
        o.hreflang.forEach((lang, j) => {
          let href = '';
          switch (o.urlLangType) {
            case 'subpage': href = `${o.baseUrl}/${o.baseUrlLang[j]}`; break;
            case 'domain': href = o.baseUrlLang[j]; break;
            default: href = o.baseUrl; break;
          }
          data += `
      <xhtml:link rel="alternate" hreflang="${clean(lang)}" href="${clean(href + file.loc).replace(/\/{2,}/, '/')}/" />`;
        });
        data += `
   </${type}>`;
        return data;
      }
      function addToRobotsTxt(sitemap, o) {
        // console.log('Will add sitemap if robotsTxt is true:', o.robotsTxt)
        if (o.robotsTxt) {
          fs.appendFileSync(`${o.dir}/robots.txt`, `Sitemap: ${o.baseUrl}/${sitemap}`);
        }
      }
      options = Object.assign(defaults, options);
      console.log(JSON.stringify(options));
      if (!fs.existsSync(options.dir)) {
        console.log(fs.readdirSync('./'));
        throw options.dir + ' doesn\'t exist!'
      }
      if (!options.baseUrl.match(/^http/)) {
        console.warn('baseUrl missing protocol!');
        options.baseUrl = ('http://' + options.baseUrl).replace(/\/{3,}/, '//');
      }
      // console.log(instance.nodeIndex);
      let nodes = [];
      instance.nodeIndex.forEach(node => {
        nodes.push(node);
      })
      // console.log(JSON.stringify(nodes.map(node => ({ path: node.path, dir: node.file.stat.isDirectory(), exclude: options.exclude.includes(node.path), underscore: node.path.split('/').find(v => v.match(/^_/)), include: (options.include.length == 0 || options.include.includes(node.path)), shortEnough: (options.baseUrl + node.path).length <= 2048 }))));
      let files = [...nodes.filter(node => !node.file.stat.isDirectory() && !options.exclude.includes(node.path) && !node.path.split('/').find(v => v.match(/^_/)) && node.name != undefined && (options.include.length == 0 || options.include.includes(node.path)) && (options.baseUrl + node.path).length <= 2048).map(node => ({
        loc: node.path.replace(/\/index\b/g, '/'),
        lastmod: node.file.stat.mtime,
      })), ...options.static.filter(file => (options.baseUrl + file.path).length <= 2048)];
      // console.log(JSON.stringify(files));
      let fileArrs = [];
      for (let i = 0, len = files.length; i < len; i += 50000) {
        // console.log('Pushing files to fileArrs');
        fileArrs.push(files.slice(i, i + 50000));
      }
      const datastart = `<?xml version="1.0" encoding="UTF-8"?>`;
      let dataindex = datastart + `
<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`;
      fileArrs.forEach((arr, i) => {
        // console.log('Interating through each item of fileArrs');
        if (i >= 50000) return;
        let data = datastart + `
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">`;
        for (file of arr) {
          // console.log('Iterating through each URL');
          let url = genUrl(file, options, 'url');
          if (Buffer.byteLength(data + url + `
</urlset>`) >= 52428800) {
            fileArrs.at(-1) = [...fileArrs.at(-1), ...arr.slice(arr.indexOf(file))];
            if (fileArrs.at(-1).length < 50000) {
              fileArrs.push(fileArrs.at(-1).slice(50000));
              fileArrs.at(-1) = fileArrs.slice(0, 50000);
            }
            break;
          };
          data += url;
        }
        let sitemap = 'sitemap';
        if (fileArrs.length > 1) {
          sitemap += '-' + i;
          dataindex += genUrl({ loc: `/${sitemap}.xml` }, options, 'sitemap');
        } else {
          addToRobotsTxt(sitemap + '.xml', options);
        }
        write(`${options.dir}/${sitemap}.xml`, data + `
</urlset>`);
      });
      if (fileArrs.length > 1) {
        addToRobotsTxt('sitemap-index.xml', options);
        write(`${options.dir}/sitemap-index.xml`, dataindex + `
</sitemapindex>`);
      }
      console.log('routify-plugin-sitemap finished');
    }
  }
};
