#!/bin/bash
cd advanced_test || exit 1
ls ../../../routify-plugin-sitemap/
yarn add --dev "file:../../../routify-plugin-sitemap/"
# npm install routify-plugin-sitemap --save-dev
yarn install
mkdir public
sed -i -E "s/(import \{ defineConfig \} from 'vite')/\1\nimport genSitemap from 'routify-plugin-sitemap';/g" vite.config.js
sed -i -E "s/routify\(\)/routify(\{plugins:\[genSitemap\(\{baseUrl:'https:\/\/example.com', dir:'public', static: \[\{ loc: '\/staticpage', changefreq: 'monthly',     priority: '1.0', lastmod: 'November 16 2021 12:00 AM' \}\], exclude: \['\/hidden'\], hreflang: \['en-gb'\], baseUrlLang: \['uk'\], urlLangType: 'subpage', robotsTxt: true\}\)\]\}\)/g" vite.config.js
yarn build || exit 1
npx sitemap --validate dist/sitemap.xml || exit 1
cd ..
