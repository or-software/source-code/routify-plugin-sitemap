#!/bin/bash
cd basic_test || exit 1
ls ../../../routify-plugin-sitemap/
yarn add --dev "file:../../../routify-plugin-sitemap/"
# npm install routify-plugin-sitemap --save-dev
yarn install
mkdir public
sed -i -E "s/(import \{ defineConfig \} from 'vite')/\1\nimport genSitemap from 'routify-plugin-sitemap';/g" vite.config.js
sed -i -E "s/routify\(\)/routify(\{plugins:\[genSitemap\(\{baseUrl:'https:\/\/example.com', dir:'public'\}\)\]\}\)/g" vite.config.js
yarn build || exit 1
npx sitemap --validate dist/sitemap.xml || exit 1
cd ..
