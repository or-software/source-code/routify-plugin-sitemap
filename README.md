# routify-plugin-sitemap

This plugin generates a sitemap.xml file for your website that makes use of Routify.

## Usage
In your config insert:

```js
import genSitemap from 'routify-plugin-sitemap';
// ...
  routify({
      plugins: [genSitemap({
        baseUrl: 'https://example.com'
      })]
    })
```
Advanced example:
```js
  routify({
      plugins: [genSitemap({
        baseUrl: 'https://example.com',
        dir: 'public',
        static: [
          {
            loc: '/staticpage',
            changefreq: 'monthly',
            priority: '1.0',
            lastmod: 'November 16 2021 12:00 AM'
          }
        ],
        exclude: ['/hidden'],
        hreflang: ['en-gb'],
        baseUrlLang: ['uk'],
        urlLangType: 'subpage',
        robotsTxt: true
      })]
    })
```

## Options
`baseUrl` is the only required option. All other options are optional.
### `baseUrl`
Set this option to insert your domain before a page's path. A page's path begins with a slash, like `/about`. Thus `baseUrl` should not end in a slash. So if your domain is `https://example.com`, and one of your pages is at `/about`, it will appear in `sitemap.xml` as `https://example.com/about`.
### `dir`
Set this to the directory in your project you would place static files. By default, this is set to `public`. 
### `static`
This is an array of objects, each equivalent to a sitemap's url entry. It accepts as properties `loc`, `priority`, `lastmod`, and `changefreq`. `loc` should be in the format `/page`; it is prefixed with `baseUrl`. `lastmod` must only be parsable by JavaScript's Date class.
### `exclude`
This is an array of strings, each one the path to a file created by Svelte/Routify that you do not want to be added to the sitemap. Format each path like `/path`. Basically a blacklist.
### `include`
This is an array of strings, each one the path to a file created by Svelte/Routify that you only want to be added to the sitemap. Format each path like `/path`. Basically a whitelist. Paths in both `exclude` and `include` will be excluded. Paths in `static` but not in `include` are still included.
### `hreflang`
This is an array of strings, each one should be a valid [language/region code](https://developers.google.com/search/docs/advanced/crawling/localized-versions?visit_id=637726685939047208-1808987121&rd=1#language-codes).
### `baseUrlLang`
This is an array of strings, each one corresponding to the value in `hreflang` of the same index. This is the string used to modify `baseUrl`.
### `urlLangType`
This is a string which can be either of these values:
* `subpage` - appends `baseUrlLang` to `baseUrl` as a subpage ('https://www.example.com/uk/')
* `domain` - replaces `baseUrl`. ('https://www.example.co.uk/')
### `robotsTxt`
This option, when set to `true`, causes the plugin to append a line containing the Sitemap URL to `robots.txt`. Default is `false`. If `robots.txt` does not exist, it will be created.
## Tips
Check out Search Engine Journal's [article](https://www.searchenginejournal.com/technical-seo/xml-sitemaps/) about sitemap.xml files to learn more.
